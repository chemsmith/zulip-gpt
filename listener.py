# A small application to proxy conversations between zulip and gpt-3.5

import os
import threading
import time

import openai
import tiktoken
import zulip

# Message cache to hold messages that have already been processed
# topic -> [{role: "system", content: "Initial prompt"}, {role: "user", content: "Hi"}, {role: "assistant", content: "Hello"}]
MESSAGE_HISTORY_CACHE = {}
history_lock = threading.Lock()

# Zulip API

client = zulip.Client()

GPT_CHAT_STREAM = os.environ.get("GPT_BOT_STREAM") or "ChatGPT"

# OpenAI API

openai.api_key = os.environ["OPENAI_API_KEY"]

MODEL = "gpt-4o"
INITIAL_PROMPT = """
You are an assistant in a multi-person chat.
Different people will be identified by an email at the start of the prompt, for example `email@domain.com: This is a prompt.`.
Only use peoples names if required to address a specific person. Be consise and not unnecessarily friendly.
"""


def listen_for_messages():
    print("Listening for messages")
    client.call_on_each_message(lambda msg: threading.Thread(target=handle_message, args=[msg]).start())


def handle_message(msg):
    if msg["type"] == "stream":
        topic = msg["subject"]
        
        if topic not in MESSAGE_HISTORY_CACHE:
            fillMessageHistory(topic)

        if msg["sender_email"] != client.email:
            # Add the message to the cache
            with history_lock:
                MESSAGE_HISTORY_CACHE[topic].append(
                    {"role": "user", "content": f'{msg["sender_email"]}: {msg["content"]}'})
            
            response_text = ""

            response_message = client.send_message({
                "type": "stream",
                "to": GPT_CHAT_STREAM,
                "topic": topic,
                "content": "*hmmm*"
            })

            # Stream response from model
            response = openai.ChatCompletion.create(
                model=MODEL,
                messages=MESSAGE_HISTORY_CACHE[topic],
                stream=True)

            # Stream response to zulip with repeated editing of the same message
            last_message_time = time.time()
            for chunk in response:
                if chunk["choices"][0]["finish_reason"]:
                    break
                if 'content' in chunk["choices"][0]["delta"]:
                    response_text += chunk["choices"][0]["delta"]["content"]

                if len(response_text) > 0 and time.time() - last_message_time > 1:
                    client.update_message({
                        "message_id": response_message["id"],
                        "content": response_text
                    })
                    last_message_time = time.time()

            # Update the message with the final response
            client.update_message({
                "message_id": response_message["id"],
                "content": response_text
            })

            # Add the response to the cache
            with history_lock:
                MESSAGE_HISTORY_CACHE[topic].append(
                    {"role": "assistant", "content": response_text})


def fillMessageHistory(topic, token_limit=1000):
    history = client.get_messages({"anchor": "newest", "num_before": 100, "num_after": 0, "apply_markdown": False, "narrow": [
                                  ["stream", GPT_CHAT_STREAM], [ "topic", topic]]})
    messages = []

    topic_content = f'The subject line for this conversation is "{topic}"'

    tokens = num_tokens_from_string(INITIAL_PROMPT + topic_content)
    for message in history["messages"]:
        tokens += num_tokens_from_string(message["content"])
        if tokens > token_limit:
            break
        if message["sender_email"] == client.email and message["content"] != "*hmmm*":
            messages.append(
                {"role": "assistant", "content": message["content"]})
        else:
            messages.append(
                {"role": "user", "content": f'{message["sender_email"]}: {message["content"]}'})

    with history_lock:
        MESSAGE_HISTORY_CACHE[topic] = [
            {"role": "system", "content": INITIAL_PROMPT},
            {"role": "system", "content": topic_content},
            *messages
        ]


def num_tokens_from_string(string: str) -> int:
    encoding = tiktoken.encoding_for_model(MODEL)
    num_tokens = len(encoding.encode(string))

    return num_tokens


def main():
    listen_for_messages()


if __name__ == "__main__":
    main()

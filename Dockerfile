FROM python:3.13-slim-bookworm

WORKDIR /app

COPY listener.py requirements.txt ./

RUN apt update && \
    apt install -yqq build-essential && \
    pip install --upgrade pip && \
    pip install -r requirements.txt && \
    apt remove -yqq build-essential

CMD [ "python", "listener.py" ]
